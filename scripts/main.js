$(document).ready(function(){

  if($('body').attr('id') == 'reportBody') {//in order not to be executed in pages other than report page
                                            //in real project, I would create seperate file instead of this if condition
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Months');
      data.addColumn('number', 'Expenditure Level');

      data.addRows([
        ['Jan', 3500],
        ['Feb', 2000],
        ['March', 7000],
        ['April', 1000],
        ['May', 3000],
        ['June', 5200],
        ['July', 5000],
        ['Aug', 2200],
        ['Sep', 9999],
        ['Oct', 8000],
        ['Nov', 4000],
        ['Dec', 6000]
      ]);

      var options = {
        title: 'Expenditure Level Throughout the Year',
        hAxis: {
          title: 'Months',
          //format: 'h:mm a',
          viewWindow: {
            min: [7, 30, 0],
            max: [17, 30, 0]
          }
        },
        vAxis: {
          title: 'Amounts are in USD'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('report'));

      chart.draw(data, options);
    }
  }
});